# Development environment

To work on the app you want to be able to preview changes quickly and not wait for Docker images to
build. That's what this is for.

Just run
```sh
$ docker-compose up
```

Start the backend on port `3000` and frontend on port `8888` and you're good to go. To access the
frontend go to `http://localhost:8080` and to access the backend go to `http://localhost:8080/api`.

This is very similar to how the app is deployed so there wont be much difference between deployment
and development environment. If you want to use different ports just change the nginx config.

### Note

For development, you'll still need to setup Google OAuth Service correctly.

## Contact

### Maintainers
[Dušan Simić](https://github.com/dusansimic)